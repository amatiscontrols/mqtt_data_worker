import paho.mqtt.client as mqtt
import os, ujson, threading, queue, uuid

class MQTTClient():
    def __init__(self, config_file):
        self.cwd = os.path.abspath(os.path.dirname(__file__))
        self.config_data = self.get_config_data(config_file)
        self.message_queue = queue.Queue(maxsize=5000)
        self.mqtt_cloud_client = None
        self.initial_connect = False
        self.cloud_connected = False
        self.connect_mqtt_cloud()

    def get_config_data(self, config_file):
        config_file_path = self.cwd + "/{}".format(config_file)
        with open(config_file_path, "r") as conf_file:
            config_data = ujson.load(conf_file)
            conf_file.close()
        return config_data

    def connect_mqtt_cloud(self):
        while not self.initial_connect:
            client_id = "{}_{}".format(self.config_data["client_id_cloud"], uuid.uuid4())
            amatis_client = mqtt.Client(client_id,clean_session=True,transport="websockets",protocol=mqtt.MQTTv31)
            amatis_client.username_pw_set(self.config_data["mqtt_username_cloud"],self.config_data["mqtt_password_cloud"])
            amatis_client.tls_set(ca_certs=self.cwd + "/ca-certificates.crt")
            amatis_client.on_connect = self.on_connect_cloud
            amatis_client.on_message = self.on_message_generic_cloud
            amatis_client.on_disconnect = self.on_disconnect_cloud
            amatis_client.on_log = self.on_log_cloud

            connect_result = amatis_client.connect(self.config_data["mqtt_host_cloud"], self.config_data["mqtt_port_cloud"], 60)
            if connect_result == 0:
                self.initial_connect = True
                self.cloud_mqtt_client = amatis_client
                self.cloud_mqtt_client.loop_start()

    def on_message_generic_cloud(self, client, userdata, message):
        """
        Callback triggered on message receipt, currently used for debug but this can be configured
        to trigger actions on AMBR in the future

        Arguments:
            client -- [client object, passed automatically by paho mqtt client object]
            userdata -- [client property, passed automatically by paho mqtt client object]
            message -- [message that triggered the callback, passed automatically by paho mqtt client object]
        """
        # print("GENERIC ON MESSAGE - Cloud - {}".format(message.payload))
        self.message_queue.put(message.payload)

    def on_connect_cloud(self, client, userdata, flags, rc):
        """[summary]
        Callback triggered on client connection to broker, subscription settings should be configured here
        to trigger actions on AMBR in the future

        Arguments:
            client -- [client object, passed automatically by paho mqtt client object]
            userdata  -- [client property, passed automatically by paho mqtt client object]
            flags -- [client connection property based upon connection response, passed automatically by paho mqtt client object]
            rc -- [client connection property, response code returned by the broker on connect attempt, passed automatically by paho mqtt client object]
        """
        print("Cloud client connected with result code '{}'".format(str(rc)))
        for topic in self.config_data["topics"]:
            print("Subscribing to 'sites/{}/{}'".format(self.config_data["site_id"], topic))
            client.subscribe("sites/{}/{}".format(self.config_data["site_id"], topic), qos = 1)
        self.cloud_connected = True

    def on_disconnect_cloud(self, client, userdata, rc):
        """
        Callback triggered on client disconnect from broker. The client will attempt to reconnect automatically
        but additional handling can be added here

        Arguments:
            client -- [client object, passed automatically by paho mqtt client object]
            userdata  -- [client property, passed automatically by paho mqtt client object]
            rc -- [client connection property, response code returned by the broker on disconnect, passed automatically by paho mqtt client object]
        """
        # if rc != 0:
        #     self.log.info("Unexpected disconnection.")
        print("Disconnected from broker")
        self.cloud_connected = False

    def on_log_cloud(self, mqttc, obj, level, string):
        """[summary]
        Callback triggered on client log, allows for logging to be piped to another location

        Arguments:
            mqttc -- [client object, passed automatically by paho mqtt client object]
            obj  -- [client property, passed automatically by paho mqtt client object]
            level -- [client property, indicates logging level, passed automatically by paho mqtt client object]
            string -- [payload for log, passed automatically by paho mqtt client object]
        """
        # print("Cloud Client on_log: {}".format(string))
        pass