## Description
This is a class that uses an MQTT client to look for link_ids, and perform actions when it hears them.

## CONFIG FILE
Use this file to determine which site data will be streamed from, and which topics will be subscribed to.


1. **"site_id"** - 3 character site id
1. **"client_id_cloud"** - name of client id, can be anything
1. **"mqtt_host_cloud"** - mqtt host site
1. **"mqtt_username_cloud"** - username for accessing the mqtt streams
1. **"mqtt_password_cloud"** - password for accessing the mqtt streams
1. **"mqtt_port_cloud"** - port for accessing the mqtt streams
1. **"topics"** - mqtt topics to subscribe to, in list format

--- 
## Instantiating the class

**Required params:** config file path (update to the actual config file path when calling)
   - Example:
      ```py
      M1 = MQTTDataWorker(config_file="-- config file --", scene_file="-- scene file --")
      ```
## Using the class

When instantiated, this class will create an mqtt client. Within that client, all messages that are sent to the topics selected in the config file will be added to the client's message queue. From there, this class can be altered to do whatever operation is desired upon reciept of certain messages.

### MQTT_LISTENER METHOD
This method is run upon instantiation. When messages are added to the mqtt client's message queue, this method receives them, and manages what happenes to them. After actions are performed, the message is removed from the queue.
```python
...
message = self.mqtt_client.message_queue.get()
message = ujson.loads(message.decode("utf-8"))
...
```
After the message is loaded with json, it can then be parsed and any action can be performed on it. Whether or not an action is taken on a message, the message must be removed from the queue.
```python
self.mqtt_client.message_queue.task_done()
```
