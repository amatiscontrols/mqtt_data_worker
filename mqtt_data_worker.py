from mqtt_client import MQTTClient
import time, ujson, os


class MQTTDataWorker():
    def __init__(self, config_file, scene_file):
        """Given a config file, this class has the required methods to subscribe to an MQTT data stream and listen for messages. The additional message parsing methods require
        a scene file containing the scene IDs for the on and off scenes in each location.

        Args:
            config_file (string): JSON file including site_id, client_id, host, username, password, port, and topics.
            scene_file (string): JSON file including scene IDs for the on and off scenes in each location.
        """
        self.cwd = os.path.abspath(os.path.dirname(__file__))
        self.mqtt_client = MQTTClient(config_file)
        self.config_data = self.get_config_data(config_file)
        self.scene_data = self.get_config_data(scene_file)

    def get_config_data(self, config_file):
        config_file_path = self.cwd + "/{}".format(config_file)
        with open(config_file_path, "r") as conf_file:
            config_data = ujson.load(conf_file)
            conf_file.close()
        return config_data

    # LISTENER METHOD

    def mqtt_listener(self, read_frequency=0.01):
        while True:
            time.sleep(read_frequency)
            if self.mqtt_client.message_queue.empty():
                continue
            else:
                message = self.mqtt_client.message_queue.get()
                message = ujson.loads(message.decode("utf-8"))
                self.check_override(message)
                self.mqtt_client.message_queue.task_done()

    # MESSAGE PARSING METHODS

    def check_override(self, message):
        try:
            if "15aa" not in message["data"]["from_device"]:
                location, state = self.get_scene(message["data"]["data"][0]["cval"])
                print("{} switch pressed in {}".format(state, location))
        except:
            pass

    def get_scene(self, link_id):
        if link_id in self.scene_data:
            return self.scene_data[link_id]["location"], self.scene_data[link_id]["scene"]